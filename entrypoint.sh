#!/usr/bin/env bash

# Mandatory globals
TEMPLATE=${TEMPLATE:? 'template is missing'}

#optional globals
OUTPUT_ARTIFACT=${OUTPUT_ARTIFACT:= 'manifest.yml'}

IMAGE_FILE=${BITBUCKET_PIPE_SHARED_STORAGE_DIR}/yurii_tatur/pipe-ecr/image_name
if test -f "$IMAGE_FILE"; then
    export IMAGE=`cat ${IMAGE_FILE}`
fi

init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      export `eval echo '$'${array_var}_${i}`
    done
}
init_array_var 'EXTRA_ARGS'

source "/common.sh"

info 'Start building artifacts'
envsubst < ${TEMPLATE} > ./${OUTPUT_ARTIFACT}
chmod 777 ./${OUTPUT_ARTIFACT}
success 'Pipe finished'
